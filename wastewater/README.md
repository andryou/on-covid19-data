# on-covid19-data / wastewater

Auto-updated mirror of COVID-19 Wastewater Surveillance in Ontario, with the ability to track historical changes.

## COVID-19 Wastewater Surveillance in Ontario

| Region (File Size) | Description | Last Updated | References |
|---|---|---|---|
| [**Ontario**](https://gitlab.com/andryou/on-covid19-data/-/raw/main/wastewater/province-wastewater-graphic.png) (67.8 KB) | Province-Wide | Thu Aug  1 16:27:02 2024 | [Commit History](https://gitlab.com/andryou/on-covid19-data/-/commits/main/wastewater/province-wastewater-graphic.png) - [Source File](https://www.publichealthontario.ca/-/media/Images/OPHESAC-Science-Table/COVID-19-Wastewater-Signals/en/province-wastewater-graphic.png) |
| [**North West**](https://gitlab.com/andryou/on-covid19-data/-/raw/main/wastewater/north-west-wastewater-graphic.png) (75.1 KB) | Northwestern Health Unit and Thunder Bay District Health Unit | Thu Aug  1 16:27:02 2024 | [Commit History](https://gitlab.com/andryou/on-covid19-data/-/commits/main/wastewater/north-west-wastewater-graphic.png) - [Source File](https://www.publichealthontario.ca/-/media/Images/OPHESAC-Science-Table/COVID-19-Wastewater-Signals/en/north-west-wastewater-graphic.png) |
| [**North East**](https://gitlab.com/andryou/on-covid19-data/-/raw/main/wastewater/north-east-wastewater-graphic.png) (64.8 KB) | Algoma Public Health; North Bay Parry Sound District Health Unit; Porcupine Health Unit; Public Health Sudbury & Districts; and Timiskaming Health Unit | Thu Aug  1 16:27:03 2024 | [Commit History](https://gitlab.com/andryou/on-covid19-data/-/commits/main/wastewater/north-east-wastewater-graphic.png) - [Source File](https://www.publichealthontario.ca/-/media/Images/OPHESAC-Science-Table/COVID-19-Wastewater-Signals/en/north-east-wastewater-graphic.png) |
| [**Central East, excluding GTA**](https://gitlab.com/andryou/on-covid19-data/-/raw/main/wastewater/central-east-wastewater-graphic.png) (73.1 KB) | Haliburton Kawartha and Pine Ridge; Peterborough; and Simcoe Muskoka | Thu Aug  1 16:27:04 2024 | [Commit History](https://gitlab.com/andryou/on-covid19-data/-/commits/main/wastewater/central-east-wastewater-graphic.png) - [Source File](https://www.publichealthontario.ca/-/media/Images/OPHESAC-Science-Table/COVID-19-Wastewater-Signals/en/central-east-wastewater-graphic.png) |
| [**Eastern**](https://gitlab.com/andryou/on-covid19-data/-/raw/main/wastewater/eastern-wastewater-graphic.png) (69.3 KB) | Eastern Ontario; Hastings Prince Edward; Kingston, Frontenac and Lennox & Addington; Leeds, Grenville & Lanark; Ottawa; and Renfrew County and District | Thu Aug  1 16:27:04 2024 | [Commit History](https://gitlab.com/andryou/on-covid19-data/-/commits/main/wastewater/eastern-wastewater-graphic.png) - [Source File](https://www.publichealthontario.ca/-/media/Images/OPHESAC-Science-Table/COVID-19-Wastewater-Signals/en/eastern-wastewater-graphic.png) |
| [**South West**](https://gitlab.com/andryou/on-covid19-data/-/raw/main/wastewater/south-west-wastewater-graphic.png) (70.1 KB) | Chatham-Kent; Grey Bruce; Huron Perth; Lambton; Middlesex-London; Southwestern; and Windsor-Essex | Thu Aug  1 16:27:05 2024 | [Commit History](https://gitlab.com/andryou/on-covid19-data/-/commits/main/wastewater/south-west-wastewater-graphic.png) - [Source File](https://www.publichealthontario.ca/-/media/Images/OPHESAC-Science-Table/COVID-19-Wastewater-Signals/en/south-west-wastewater-graphic.png) |
| [**GTA**](https://gitlab.com/andryou/on-covid19-data/-/raw/main/wastewater/gta-wastewater-graphic.png) (69.3 KB) | Durham Region; Halton Region; Peel; Toronto; and York Region | Thu Aug  1 16:27:06 2024 | [Commit History](https://gitlab.com/andryou/on-covid19-data/-/commits/main/wastewater/gta-wastewater-graphic.png) - [Source File](https://www.publichealthontario.ca/-/media/Images/OPHESAC-Science-Table/COVID-19-Wastewater-Signals/en/gta-wastewater-graphic.png) |
| [**Central West, excluding GTA**](https://gitlab.com/andryou/on-covid19-data/-/raw/main/wastewater/central-west-wastewater-graphic.png) (68.9 KB) | Brant County; Haldimand-Norfolk; Hamilton; Niagara Region, Waterloo; and Wellington-Dufferin-Guelph | Thu Aug  1 16:27:06 2024 | [Commit History](https://gitlab.com/andryou/on-covid19-data/-/commits/main/wastewater/central-west-wastewater-graphic.png) - [Source File](https://www.publichealthontario.ca/-/media/Images/OPHESAC-Science-Table/COVID-19-Wastewater-Signals/en/central-west-wastewater-graphic.png) |

Source: Ontario Agency for Health Protection and Promotion (Public Health Ontario). COVID-19 wastewater surveillance in Ontario. Toronto, ON: King's Printer for Ontario; 2022 - https://www.publichealthontario.ca/en/Data-and-Analysis/Infectious-Disease/COVID-19-Data-Surveillance/Wastewater

## About

Maintained by [https://twitter.com/andryou](https://twitter.com/andryou)

Powered by a script developed to use minimal resources/bandwidth of the www.publichealthontario.ca server, with a couple of validation checks:

1. An HTTP "HEAD" request method sent to retrieve only the headers (not content) of each file, and if HTTP status code 200 (no errors) the Last-Modified header is compared against a locally stored last known good value
2. If different, the file contents are downloaded, and if HTTP status code 200 (no errors) then the file is compared against a locally stored last known good copy of the file
3. If different, the new file is mirrored and notifications sent out

If at any point a 5xx error (server error) is encountered, the script terminates itself.